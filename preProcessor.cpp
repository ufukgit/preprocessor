#include <iostream>
#include <cstring>
#include <conio.h>
#include <stdio.h>
using namespace std;
#line 3

#define MAK01(id1, id2) id1+id2         
#define MAK02(cp1, cp2) strcpy(cp1, cp2)
#define MAK03(cp1, cp2) strcat(cp1, cp2)
#define MAK04(id1) fonk(id1) 
#define MAK05(ide1, ide2) ((ide1<ide2) && (ide1>5) && (ide1%2==0)) ? 1 : 0
#define MAKR01 10
#define MAKR02 35
#define MAK07 3
#define MAKEXAM1 5
void fonk(int id1);

int main(void)
{
  int id11;

  cout << "Bir int deger giriniz: ";
  cin >> id11;

//  #error Programda hata var! // 1
  cout << "Girdiginiz Deger : " << id11 << "\n";
//  cout << __LINE__ << "\n";
  cout << "\n\n***************" << endl;

  char cdizi[50];

  MAK02(cdizi, "Ufuk");

  cout << "Toplanacak Iki Deger 21 ve 62 " << "\n";

  cout << "Toplam : " << MAK01(21, 62) << "\n" << "Isminiz : " << MAK03(cdizi, " Mentes") << "\n";
  cout << "\n\n***************" << endl;

  MAK04(42);
  cout << "\n\n***************" << endl;

  int ide1, ide2;

  cout << "Bir int deger giriniz MAKR01 : ";
  cin >> ide1;

  cout << "Bir int deger daha giriniz MAKR02: ";
  cin >> ide2;

  cout << "Makro besin sonucu true or false :" << MAK05(ide1, ide2);
  
  cout << "\n\n***************" << endl;
  #if MAKR01<MAKR02
     cout << "MAKR01 degeri MAKR02 degerinden kucuktur." << "\n";
     cout << "MAKR01 degeri: " << MAKR01 << "\n";
     cout << "MAKR02 degeri: " << MAKR02 << "\n";
  #endif

//   A�a��da yer alan i�lem sat�rlar� derleyici taraf�ndan derlenmez.
  #if MAKR01>MAKR02
     cout << "MAKR01 degeri MAKR02 degerinden buyuktur." << "\n";
     cout << "MAKR01 degeri: " << MAKR01 << "\n";
     cout << "MAKR02 degeri: " << MAKR02 << "\n";
  #endif
 
   cout << "\n\n***************" << endl;

  int id;

  #if MAK07 == 1
    for (id=1; id<=10; id++) cout << id << " ";
    cout << endl;
  #elif MAK07 == 2
    for (id=2; id<=10; id++) cout << id << " ";
    cout << endl;
  #elif MAK07 == 3
    for (id=3; id<=10; id++) cout << id << " ";
	cout << endl;
  #endif


    cout << "MAKEXAM1 degeri: " <<    MAKEXAM1;;
  return 0;
}

void fonk(int id1)
{
  int id2;

  cout << "10 kere yazdirilacak deger : ";
  for (id2=0; id2<10; id2++) cout << id1 << " ";
  cout << endl;
}
